﻿namespace TBSEAssignment
{
    partial class DeleteContentFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.currentRadioBtn = new System.Windows.Forms.RadioButton();
            this.allRadioBtn = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.deleteTxt = new System.Windows.Forms.TextBox();
            this.ignoreBox = new System.Windows.Forms.CheckBox();
            this.submitBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.allRadioBtn);
            this.groupBox1.Controls.Add(this.currentRadioBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 50);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Files";
            // 
            // currentRadioBtn
            // 
            this.currentRadioBtn.AutoSize = true;
            this.currentRadioBtn.Checked = true;
            this.currentRadioBtn.Location = new System.Drawing.Point(6, 19);
            this.currentRadioBtn.Name = "currentRadioBtn";
            this.currentRadioBtn.Size = new System.Drawing.Size(111, 17);
            this.currentRadioBtn.TabIndex = 1;
            this.currentRadioBtn.TabStop = true;
            this.currentRadioBtn.Text = "Current Document";
            this.currentRadioBtn.UseVisualStyleBackColor = true;
            // 
            // allRadioBtn
            // 
            this.allRadioBtn.AutoSize = true;
            this.allRadioBtn.Location = new System.Drawing.Point(123, 19);
            this.allRadioBtn.Name = "allRadioBtn";
            this.allRadioBtn.Size = new System.Drawing.Size(93, 17);
            this.allRadioBtn.TabIndex = 2;
            this.allRadioBtn.Text = "All Documents";
            this.allRadioBtn.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ignoreBox);
            this.groupBox2.Controls.Add(this.deleteTxt);
            this.groupBox2.Location = new System.Drawing.Point(12, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 72);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Text";
            // 
            // deleteTxt
            // 
            this.deleteTxt.Location = new System.Drawing.Point(6, 19);
            this.deleteTxt.Name = "deleteTxt";
            this.deleteTxt.Size = new System.Drawing.Size(248, 20);
            this.deleteTxt.TabIndex = 0;
            // 
            // ignoreBox
            // 
            this.ignoreBox.AutoSize = true;
            this.ignoreBox.Location = new System.Drawing.Point(6, 49);
            this.ignoreBox.Name = "ignoreBox";
            this.ignoreBox.Size = new System.Drawing.Size(83, 17);
            this.ignoreBox.TabIndex = 1;
            this.ignoreBox.Text = "Ignore Case";
            this.ignoreBox.UseVisualStyleBackColor = true;
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(12, 146);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(260, 23);
            this.submitBtn.TabIndex = 2;
            this.submitBtn.Text = "Delete";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // DeleteContentFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 180);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "DeleteContentFrm";
            this.Text = "Delete Content";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton allRadioBtn;
        private System.Windows.Forms.RadioButton currentRadioBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ignoreBox;
        private System.Windows.Forms.TextBox deleteTxt;
        private System.Windows.Forms.Button submitBtn;
    }
}