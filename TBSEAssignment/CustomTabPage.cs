﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    abstract class CustomTabPage : TabPage, ITab
    {
        protected string mFilePath;

        protected Enum mTabType;

        protected bool mFileState = false;

        protected int mSaveCount = 0;

        public CustomTabPage(String mFilePath, Enum mType)
        {
            this.mFilePath = mFilePath;
            this.Text = Path.GetFileName(mFilePath) + "               ";
            this.mTabType = mType;
            mInitializeTab();
        }
        public abstract void mInitializeTab();
        public abstract void mSaveFile();
        public abstract void mAddFormatting(FontStyle mStyle);
        public abstract void mAddText(string mText, int mColumnNumber, int mRowCount);
        public abstract void mCloseFile();
        public abstract void mDecrypt(byte[] mKey, byte[] mIV);
        public abstract void mFindAndReplace(string mText, bool mIgnoreCase, string mReplacingText);
        public abstract void mEncrypt(byte[] mKey, byte[] mIV);
        public abstract void mFormatText(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline);
        public abstract void mReadFile();
        public abstract void mCreateFile();
        public abstract string[,] mCopyText();
        public abstract void mPasteText(string[,] mStringArray);
        public abstract string mCopySingle();
        public abstract void mPasteSingle(string mCopyString);

        public string mGetFilePath()
        {
            return mFilePath;
        }

        public bool mGetFileState()
        {
            return mFileState;
        }

        public Enum mGetTabType()
        {
            return mTabType;
        }
    }
}
