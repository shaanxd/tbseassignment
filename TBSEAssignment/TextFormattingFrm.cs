﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    public partial class TextFormattingFrm : Form
    {
        IMainFrm mFrm;
        public TextFormattingFrm(IMainFrm mFrm)
        {
            InitializeComponent();
            this.mFrm = mFrm;
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            if (formattingTxt.Text.Equals(""))
            {
                MessageBox.Show("Textbox is empty");
            }
            else
            {
                if (boldBox.Checked || italicBox.Checked || underlineBox.Checked)
                {
                    if (currentRadioBtn.Checked)
                    {
                        mFrm.mApplyFormatting(formattingTxt.Text,
                            ignoreBox.Checked, boldBox.Checked, italicBox.Checked, underlineBox.Checked);
                    }
                    else
                    {
                        mFrm.mApplyFormattingToAll(formattingTxt.Text, 
                            ignoreBox.Checked, boldBox.Checked, italicBox.Checked, underlineBox.Checked);
                    }
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Please select a formatting");
                }
            }
        }
    }
}
