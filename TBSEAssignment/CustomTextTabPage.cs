﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    abstract class CustomTextTabPage : CustomTabPage
    {
        protected RichTextBox vRichTextBox;

        public CustomTextTabPage(String mFilePath, Enum mTabType) : base(mFilePath, mTabType)
        {

        }

        public override void mInitializeTab()
        {
            this.Padding = new Padding(20, 20, 20, 20);
            this.BackColor = Color.White;

            vRichTextBox = new RichTextBox();
            vRichTextBox.Dock = DockStyle.Fill;
            vRichTextBox.BorderStyle = BorderStyle.None;
            this.Controls.Add(vRichTextBox);

            if (File.Exists(mFilePath))
            {
                mReadFile();
            }
            else
            {
                mCreateFile();
            }
            mFileState = true;
        }

        public override void mAddText(string mText, int mColumnNumber, int mRowCount)
        {
            vRichTextBox.Invoke(new Action(()=>{
                vRichTextBox.AppendText(mText);
            }));
        }

        public override void mFindAndReplace(string mText, bool mIgnoreCase, string mReplacingText)
        {
            int mIndex = 0;

            RichTextBoxFinds mFind;

            if (mIgnoreCase)
            {
                mFind = RichTextBoxFinds.None;
            }
            else
            {
                mFind = RichTextBoxFinds.MatchCase;
            }

            while (mIndex < vRichTextBox.Text.LastIndexOf(mText))
            {
                vRichTextBox.Find(mText, mIndex, vRichTextBox.TextLength, mFind);
                vRichTextBox.SelectedText = mReplacingText;
                mIndex = vRichTextBox.Text.IndexOf(mText, mIndex) + 1;
            }
        }

        public override void mFormatText(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline)
        {
            int mIndex = 0;

            Font mFont = new Font(vRichTextBox.Font, FontStyle.Regular);

            if (mIsBold)
            {
                mFont = new Font(vRichTextBox.Font, mFont.Style | FontStyle.Bold);
            }
            if (mIsItalic)
            {
                mFont = new Font(vRichTextBox.Font, mFont.Style | FontStyle.Italic);
            }
            if (mIsUnderline)
            {
                mFont = new Font(vRichTextBox.Font, mFont.Style | FontStyle.Underline);
            }

            while (mIndex < vRichTextBox.Text.LastIndexOf(mText))
            {
                vRichTextBox.Find(mText, mIndex, vRichTextBox.TextLength, RichTextBoxFinds.None);
                vRichTextBox.SelectionFont = mFont;
                mIndex = vRichTextBox.Text.IndexOf(mText, mIndex) + 1;
            }
        }

        public override void mEncrypt(byte[] mKey, byte[] mIV)
        {
            String mValue = vRichTextBox.Text;
            if (!Utils.mIsEncrypted(mValue))
            {
                Task<String> mFirstTask = Task.Factory.StartNew<String>(() =>
                {
                    return Utils.mEncryptString(mValue, mKey, mIV);
                });
                Task mSecondTask = mFirstTask.ContinueWith((mPrevious) =>
                {
                    vRichTextBox.Text = mPrevious.Result;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        public override void mDecrypt(byte[] mKey, byte[] mIV)
        {
            String mValue = vRichTextBox.Text;
            if (Utils.mIsEncrypted(mValue))
            {
                Task<String> mFirstTask = Task.Factory.StartNew<String>(() =>
                {
                    return Utils.mDecryptString(mValue, mKey, mIV);
                });
                Task mSecondTask = mFirstTask.ContinueWith((mPrevious) =>
                {
                    vRichTextBox.Text = mPrevious.Result;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        public override void mAddFormatting(FontStyle mStyle)
        {
            vRichTextBox.SelectionFont = new Font(vRichTextBox.SelectionFont, vRichTextBox.SelectionFont.Style ^ mStyle); ;
        }

        public override string[,] mCopyText()
        {
            string[] mStringArray = vRichTextBox.Text.Split('\n');
            string[,] mStringSet = new string[mStringArray.Length, 1];
            for (int i = 0; i < mStringArray.Length; i++)
            {
                mStringSet[i, 0] = mStringArray[i];
            }
            return mStringSet;
        }

        public override void mPasteText(string[,] mStringArray)
        {
            Task<string> mTask = Task.Factory.StartNew<string>(() =>
            {
                string mString = "";
                for (int i = 0; i < mStringArray.GetLength(0); i++)
                {
                    for (int j = 0; j < mStringArray.GetLength(1); j++)
                    {
                        mString += mStringArray[i, j];
                        mString += "    ";   
                    }
                    mString += System.Environment.NewLine;
                }
                return mString;
            });
            Task mSecondTask = mTask.ContinueWith((mPrevious) =>
            {
                vRichTextBox.Text = mPrevious.Result;

            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public override string mCopySingle()
        {
            if (!string.IsNullOrEmpty(vRichTextBox.SelectedRtf))
            {
                return vRichTextBox.SelectedText;
            }
            else
            {
                return "";
            }
        }

        public override void mPasteSingle(String mCopyString)
        {
            if (!string.IsNullOrEmpty(mCopyString))
            {
                vRichTextBox.SelectedText = mCopyString;
            }
        }
    }
}
