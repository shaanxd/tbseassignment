﻿namespace TBSEAssignment
{
    partial class FindReplaceFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.submitBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ignoreBox = new System.Windows.Forms.CheckBox();
            this.findingTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.allRadioBtn = new System.Windows.Forms.RadioButton();
            this.currentRadioBtn = new System.Windows.Forms.RadioButton();
            this.replacingTxt = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(12, 202);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(260, 23);
            this.submitBtn.TabIndex = 5;
            this.submitBtn.Text = "Replace";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ignoreBox);
            this.groupBox2.Controls.Add(this.findingTxt);
            this.groupBox2.Location = new System.Drawing.Point(12, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 72);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Find";
            // 
            // ignoreBox
            // 
            this.ignoreBox.AutoSize = true;
            this.ignoreBox.Location = new System.Drawing.Point(6, 49);
            this.ignoreBox.Name = "ignoreBox";
            this.ignoreBox.Size = new System.Drawing.Size(83, 17);
            this.ignoreBox.TabIndex = 1;
            this.ignoreBox.Text = "Ignore Case";
            this.ignoreBox.UseVisualStyleBackColor = true;
            // 
            // findingTxt
            // 
            this.findingTxt.Location = new System.Drawing.Point(6, 19);
            this.findingTxt.Name = "findingTxt";
            this.findingTxt.Size = new System.Drawing.Size(248, 20);
            this.findingTxt.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.allRadioBtn);
            this.groupBox1.Controls.Add(this.currentRadioBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 50);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Files";
            // 
            // allRadioBtn
            // 
            this.allRadioBtn.AutoSize = true;
            this.allRadioBtn.Location = new System.Drawing.Point(123, 19);
            this.allRadioBtn.Name = "allRadioBtn";
            this.allRadioBtn.Size = new System.Drawing.Size(93, 17);
            this.allRadioBtn.TabIndex = 2;
            this.allRadioBtn.Text = "All Documents";
            this.allRadioBtn.UseVisualStyleBackColor = true;
            // 
            // currentRadioBtn
            // 
            this.currentRadioBtn.AutoSize = true;
            this.currentRadioBtn.Checked = true;
            this.currentRadioBtn.Location = new System.Drawing.Point(6, 19);
            this.currentRadioBtn.Name = "currentRadioBtn";
            this.currentRadioBtn.Size = new System.Drawing.Size(111, 17);
            this.currentRadioBtn.TabIndex = 1;
            this.currentRadioBtn.TabStop = true;
            this.currentRadioBtn.Text = "Current Document";
            this.currentRadioBtn.UseVisualStyleBackColor = true;
            // 
            // replacingTxt
            // 
            this.replacingTxt.Location = new System.Drawing.Point(6, 19);
            this.replacingTxt.Name = "replacingTxt";
            this.replacingTxt.Size = new System.Drawing.Size(248, 20);
            this.replacingTxt.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.replacingTxt);
            this.groupBox3.Location = new System.Drawing.Point(12, 146);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 50);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Replace With";
            // 
            // FindReplaceFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 237);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FindReplaceFrm";
            this.Text = "Find And Replace";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ignoreBox;
        private System.Windows.Forms.TextBox findingTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton allRadioBtn;
        private System.Windows.Forms.RadioButton currentRadioBtn;
        private System.Windows.Forms.TextBox replacingTxt;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}