﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TBSEAssignment
{
    class Utils
    {
        static string[] mAlphabetArray = { "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        
        public static string mDecryptString(string mString, byte[] mKey, byte[] mIV)
        {
            try
            {
                string mText = null;

                string mReplacedString = mString.Replace("<!-enc-!>", "");

                byte[] mEncrypted = Convert.FromBase64String(mReplacedString);

                using (AesManaged mAesMngd = new AesManaged())
                {
                    mAesMngd.Key = mKey;
                    mAesMngd.IV = mIV;

                    ICryptoTransform mDecryptor = 
                        mAesMngd.CreateDecryptor(mAesMngd.Key, mAesMngd.IV);

                    using (MemoryStream mMSDecrypt = new MemoryStream(mEncrypted))
                    {
                        using (CryptoStream mCSDecrypt = 
                            new CryptoStream(mMSDecrypt, mDecryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader mSRDecrypt = new StreamReader(mCSDecrypt))
                            {
                                mText = mSRDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                return mText;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return mString;
            }
        }

        public static string mEncryptString(string mString, byte[] mKey, byte[] mIV)
        {
            try
            {
                byte[] mEncrypted;

                using (AesManaged mAesMngd = new AesManaged())
                {
                    mAesMngd.Key = mKey;
                    mAesMngd.IV = mIV;

                    ICryptoTransform mEncryptor = 
                        mAesMngd.CreateEncryptor(mAesMngd.Key, mAesMngd.IV);

                    using (MemoryStream mMSEncrypt = new MemoryStream())
                    {
                        using (CryptoStream mCSEncrypt = 
                            new CryptoStream(mMSEncrypt, mEncryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter mSWEncrypt = new StreamWriter(mCSEncrypt))
                            {
                                mSWEncrypt.Write(mString);
                            }
                            mEncrypted = mMSEncrypt.ToArray();
                        }
                    }
                }
                return "<!-enc-!>" + Convert.ToBase64String(mEncrypted);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return mString;
            }
        }

        public static string mGetCharAt(int mValue)
        {
            try
            {
                return mAlphabetArray[mValue / 26] + "" + mAlphabetArray[(mValue % 26) + 1];
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
        }

        public static bool mIsEncrypted(string mText)
        {
            if (mText.StartsWith("<!-enc-!>"))
            {
                return true;
            }
            return false;
        }
    }
}
