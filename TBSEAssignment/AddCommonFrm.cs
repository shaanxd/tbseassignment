﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    public partial class AddCommonFrm : Form
    {
        IMainFrm mFrm;

        public AddCommonFrm(IMainFrm mFrm)
        {
            InitializeComponent();
            this.mFrm = mFrm;
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            if (contentTxtBox.Text.Equals(""))
            {
                MessageBox.Show("Textbox is empty");
            }
            else
            {
                if (excelRadioBtn.Checked)
                {
                    if(!(rowCount.Value == 0 || columnNumber.Value == 0))
                    {
                        if (allRadioBtn.Checked)
                        {
                            mFrm.mAddToAllExcel(contentTxtBox.Text, 
                                (int)columnNumber.Value, (int)rowCount.Value);
                        }
                        else
                        {
                            mFrm.mAddToExcel(contentTxtBox.Text, 
                                (int)columnNumber.Value, (int)rowCount.Value);
                        }
                        this.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("Column number and Row count cannot be 0");
                    }
                }
                else
                {
                    mFrm.mAddToAllText(contentTxtBox.Text);
                    this.Dispose();
                }
            }            
        }

        private void excelRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (excelRadioBtn.Checked)
            {
                excelGrpBox.Enabled = true;
            }
            else
            {
                excelGrpBox.Enabled = false;
            }
        }
    }
}
