﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    class TextTabPage : CustomTextTabPage
    {
        public TextTabPage(string mFilePath, Enum mType) : base(mFilePath, mType)
        {

        }

        override public void mCreateFile()
        {
            File.Create(mFilePath);
        }

        override public void mReadFile()
        {
            vRichTextBox.Text = File.ReadAllText(mFilePath);
        }

        override public void mCloseFile()
        {
            if (MessageBox.Show("Do you want to close " + Path.GetFileName(mFilePath) + "?", "Close File", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
            {
                Task mTask = Task.Factory.StartNew(() =>
                {
                    if (MessageBox.Show("Do you want to save " + Path.GetFileName(mFilePath) + "?", "Save File", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        mSaveFile();
                    }
                    while (mSaveCount != 0)
                    {

                    }
                    mFileState = false;
                    Parent.Invoke(new Action(() =>
                    {
                        Parent.Controls.Remove(this);
                    }));
                });
                mTask.Wait();
            }
        }
        override public void mSaveFile()
        {
            if (mFileState)
            {
                mSaveCount++;
                vRichTextBox.Invoke(new Action(() =>
                {
                    File.WriteAllLines(mFilePath, vRichTextBox.Text.Split('\n'));
                }));
                mSaveCount--;
            }
        }
    }
}
