﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    public partial class FindReplaceFrm : Form
    {
        IMainFrm mFrm;

        public FindReplaceFrm(IMainFrm mFrm)
        {
            InitializeComponent();
            this.mFrm = mFrm;
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(findingTxt.Text) || string.IsNullOrEmpty(replacingTxt.Text))
            {
                MessageBox.Show("Text Fields Cannot Be Empty.");
            }
            else
            {
                if (currentRadioBtn.Checked)
                {
                    mFrm.mFindAndReplace(findingTxt.Text, ignoreBox.Checked, replacingTxt.Text);
                }
                else
                {
                    mFrm.mFindAndReplaceAll(findingTxt.Text, ignoreBox.Checked, replacingTxt.Text);
                }
            }
        }
    }
}
