﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSEAssignment
{
    public interface IMainFrm
    {
        void mAddToAllText(string mText);
        void mAddToAllExcel(string mText, int mColumnCount, int mRowCount);
        void mAddToExcel(string mText, int mColumnCount, int mRowCount);
        void mDeleteText(string mText, bool mIgnoreCase);
        void mDeleteAll(string mText, bool mIgnoreCase);
        void mApplyFormatting(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline);
        void mApplyFormattingToAll(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline);
        void mFindAndReplace(string mText, bool mIgnoreCase, string mReplacingText);
        void mFindAndReplaceAll(string mText, bool mIgnoreCase, string mReplacingText);
    }
}
