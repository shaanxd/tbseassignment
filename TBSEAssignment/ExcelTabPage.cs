﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace TBSEAssignment
{
    class ExcelTabPage : CustomTabPage
    {
        Excel.Application vApplication;
        Excel.Workbook vWorkbook;
        Excel.Worksheet vWorksheet;

        DataGridView mGridView;

        public ExcelTabPage(String mFilePath, Enum mType) : base(mFilePath, mType)
        {

        }

        public override void mInitializeTab()
        {
            mGridView = new DataGridView();
            mGridView.Dock = DockStyle.Fill;
            mGridView.RowHeadersWidth = 75;

            this.Controls.Add(mGridView);

            vApplication = new Excel.Application();

            if (File.Exists(mFilePath))
            {
                mReadFile();
            }
            else
            {
                mCreateFile();
            }
            mFileState = true;
            mSetRowNumbers();
        }

        public override void mCreateFile()
        {
            vWorkbook = vApplication.Workbooks.Add(Type.Missing);

            vWorksheet = vWorkbook.Sheets["Sheet1"];
            vWorksheet = vWorkbook.ActiveSheet;

            mSetColumnRowCount(0, 0);

            vWorkbook.SaveAs(mFilePath, Type.Missing, Type.Missing, 
                Type.Missing, Type.Missing, Type.Missing,
                Excel.XlSaveAsAccessMode.xlShared, Type.Missing, 
                Type.Missing, Type.Missing, Type.Missing);
        }

        public override void mReadFile()
        {
            vWorkbook = vApplication.Workbooks.Open(mFilePath);
            vWorksheet = vWorkbook.Sheets[1];

            Excel.Range mRange = vWorksheet.UsedRange;

            mSetColumnRowCount(mRange.Columns.Count, mRange.Rows.Count);

            for (int i = 1; i <= mRange.Rows.Count; i++)
            {
                for (int j = 1; j <= mRange.Columns.Count; j++)
                {
                    if (mRange.Cells[i, j] != null && mRange.Cells[i, j].Value2 != null)
                    {
                        mGridView.Rows[i - 1].Cells[j - 1].Value 
                            = mRange.Cells[i, j].Value2.ToString();
                    }
                }
            }
        }

        public void mSetColumnRowCount(int mColumnCount, int mRowCount)
        {
            if (mColumnCount < 10)
            {
                mGridView.ColumnCount = 10;
            }
            else
            {
                mGridView.ColumnCount = mColumnCount + 2;
            }
            if (mRowCount < 25)
            {
                mGridView.RowCount = 25;
            }
            else
            {
                mGridView.RowCount = mRowCount + 2;
            }
        }

        public override void mSaveFile()
        {
            if (mFileState)
            {
                mSaveCount++;
                for (int i = 0; i < mGridView.Rows.Count; i++)
                {
                    for (int j = 0; j < mGridView.Columns.Count; j++)
                    {
                        if (!string.IsNullOrEmpty(mGridView.Rows[i].Cells[j].Value as string))
                        {
                            vWorksheet.Cells[i + 1, j + 1] 
                                = mGridView.Rows[i].Cells[j].Value.ToString();
                        }
                        else
                        {
                            vWorksheet.Cells[i + 1, j + 1] = "";
                        }
                    }
                }
                vWorkbook.Save();
                mSaveCount--;
            }
        }

        public void mSetRowNumbers()
        {
            for (int i = 0; i < mGridView.Rows.Count; i++)
            {
                mGridView.Rows[i].HeaderCell.Value = (i + 1).ToString();
            }
            for (int i = 0; i < mGridView.Columns.Count; i++)
            {
                mGridView.Columns[i].HeaderText = Utils.mGetCharAt(i);
            }
            foreach (DataGridViewColumn mColumn in mGridView.Columns)
            {
                mColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
                mColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }

        public override void mCloseFile()
        {
            if (MessageBox.Show("Do you want to close " + Path.GetFileName(mFilePath) + "?", "Close Document", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
            {
                Task mTask = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        if (MessageBox.Show("Do you want to save " + Path.GetFileName(mFilePath) + "?", "Save Document", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            mSaveFile();
                        }
                        while (mSaveCount != 0)
                        {

                        }
                        mFileState = false;

                        Parent.Invoke(new System.Action(() =>
                        {
                            Parent.Controls.Remove(this);
                        }));

                        GC.WaitForPendingFinalizers();
                        Marshal.ReleaseComObject(vWorksheet);
                        vWorkbook.Close();
                        Marshal.ReleaseComObject(vWorkbook);
                        vApplication.Quit();
                        Marshal.ReleaseComObject(vApplication);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });
                mTask.Wait();
            }
        }

        public override void mAddText(string mText, int mColumnNumber, int mRowCount)
        {
            int mCol = 0;
            int mRow = 0;

            if (mColumnNumber > mGridView.ColumnCount - 1)
            {
                mCol = mGridView.ColumnCount - 1;
            }
            else
            {
                mCol = mColumnNumber - 1;
            }
            if (mRowCount > mGridView.RowCount - 1)
            {
                mRow = mGridView.RowCount - 1;
            }
            else
            {
                mRow = mRowCount;
            }

            for (int i = 0; i < mRow; i++)
            {
                mGridView.Rows[i].Cells[mCol].Value = mText;
            }

        }

        public override void mFindAndReplace(string mText, bool mIgnoreCase, string mReplacingText)
        {
            for (int i = 0; i < mGridView.Rows.Count; i++)
            {
                for (int j = 0; j < mGridView.Columns.Count; j++)
                {
                    if (!string.IsNullOrEmpty(mGridView.Rows[i].Cells[j].Value as string))
                    {
                        int mResult = string.Compare(mGridView.Rows[i].Cells[j].Value.ToString(), 
                            mText, mIgnoreCase);
                        if (mResult == 0)
                        {
                            mGridView.Rows[i].Cells[j].Value = mReplacingText;
                        }
                    }
                }
            }
        }
        public override void mFormatText(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline)
        {
            Font mFont = new Font(mGridView.Font, FontStyle.Regular);

            if (mIsBold)
            {
                mFont = new Font(mGridView.Font, mFont.Style | FontStyle.Bold);
            }
            if (mIsItalic)
            {
                mFont = new Font(mGridView.Font, mFont.Style | FontStyle.Italic);
            }
            if (mIsUnderline)
            {
                mFont = new Font(mGridView.Font, mFont.Style | FontStyle.Underline);
            }
            for (int i = 0; i < mGridView.Rows.Count - 1; i++)
            {
                for (int j = 0; j < mGridView.Columns.Count - 1; j++)
                {
                    if (!string.IsNullOrEmpty(mGridView.Rows[i].Cells[j].Value as string))
                    {
                        int mResult = string.Compare(mGridView.Rows[i].Cells[j].Value.ToString(),
                            mText, mIgnoreCase);
                        if (mResult == 0)
                        {
                            mGridView.Rows[i].Cells[j].Style.Font = mFont;
                        }
                    }
                }
            }

        }

        public override void mEncrypt(byte[] mKey, byte[] mIV)
        {
            for (int i = 0; i < mGridView.Rows.Count; i++)
            {
                for (int j = 0; j < mGridView.Columns.Count; j++)
                {
                    string mVal = mGridView.Rows[i].Cells[j].Value as string;
                    if (!string.IsNullOrEmpty(mVal) && !Utils.mIsEncrypted(mVal))
                    {
                        int mRowVal = i;
                        int mCellVal = j;
                        Task<String> mFirstTask = Task.Factory.StartNew<String>(() =>
                        {
                            return Utils.mEncryptString(mVal, mKey, mIV);
                        });
                        Task mSecondTask = mFirstTask.ContinueWith((mPrevious) =>
                        {
                            mGridView.Rows[mRowVal].Cells[mCellVal].Value = mPrevious.Result;
                        }, TaskScheduler.FromCurrentSynchronizationContext());
                    }
                }
            }
        }

        public override void mDecrypt(byte[] mKey, byte[] mIV)
        {
            for (int i = 0; i < mGridView.Rows.Count; i++)
            {
                for (int j = 0; j < mGridView.Columns.Count; j++)
                {
                    string mVal = mGridView.Rows[i].Cells[j].Value as string;
                    if (!string.IsNullOrEmpty(mVal) && Utils.mIsEncrypted(mVal))
                    {
                        int mRowVal = i;
                        int mCellVal = j;
                        Task<String> mFirstTask = Task.Factory.StartNew<String>(() =>
                        {
                            return Utils.mDecryptString(mVal, mKey, mIV);
                        });
                        Task mSecondTask = mFirstTask.ContinueWith((mPrevious) =>
                        {
                            mGridView.Rows[mRowVal].Cells[mCellVal].Value = mPrevious.Result;
                        }, TaskScheduler.FromCurrentSynchronizationContext());
                    }
                }
            }
        }

        public override void mAddFormatting(FontStyle mStyle)
        {
            Font mFont = new Font(mGridView.Font, mGridView.Font.Style ^ mStyle);
            foreach (DataGridViewCell mCell in mGridView.SelectedCells)
            {
                if (!string.IsNullOrEmpty(mCell.Value as string))
                {
                    mCell.Style.Font = mFont;
                }
            }
        }

        public override string[,] mCopyText()
        {
            string[,] mStringArray = new string[
                mGridView.RowCount - 1, mGridView.ColumnCount - 1];

            for (int i = 0; i < mGridView.Rows.Count; i++)
            {
                for (int j = 0; j < mGridView.Columns.Count; j++)
                {
                    string mVal = mGridView.Rows[i].Cells[j].Value as string;
                    if (!string.IsNullOrEmpty(mVal))
                    {
                        mStringArray[i, j] = mVal;
                    }
                    else
                    {
                        mStringArray[i, j] = "";
                    }
                }
            }
            return mStringArray;
        }

        public override void mPasteText(string[,] mStringArray)
        {
            int mRowCount = mStringArray.GetLength(0);
            int mColumnCount = mStringArray.GetLength(1);

            if (mRowCount <= mGridView.RowCount && mColumnCount <= mGridView.ColumnCount)
            {
                for (int i = 0; i < mRowCount; i++)
                {
                    for (int j = 0; j < mColumnCount; j++)
                    {
                        mGridView.Rows[i].Cells[j].Value = mStringArray[i, j];
                    }
                }
            }
            else
            {
                MessageBox.Show("Insufficient Column and Rows");
            }
        }

        public override string mCopySingle()
        {
            if (mGridView.SelectedCells.Count < 1)
            {
                return "";
            }
            else if(mGridView.SelectedCells.Count > 1)
            {
                MessageBox.Show("Please Select A Single Cell. For Documents Use Copy Options");
                return "";
            }
            else
            {
                return mGridView.SelectedCells[0].Value.ToString();
            }
        }

        public override void mPasteSingle(String mCopyString)
        {
            foreach(DataGridViewCell mCell in mGridView.SelectedCells)
            {
                mCell.Value = mCopyString;
            }
        }
    }
}
