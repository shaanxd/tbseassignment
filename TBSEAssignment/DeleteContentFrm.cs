﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TBSEAssignment
{
    public partial class DeleteContentFrm : Form
    {
        IMainFrm mFrm;

        public DeleteContentFrm(IMainFrm mFrm)
        {
            InitializeComponent();
            this.mFrm = mFrm;
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            if (!deleteTxt.Text.Equals(""))
            {
                if (currentRadioBtn.Checked)
                {
                    mFrm.mDeleteText(deleteTxt.Text, ignoreBox.Checked);
                }
                else
                {
                    mFrm.mDeleteAll(deleteTxt.Text, ignoreBox.Checked);
                }
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Textbox cannot be empty");
            }
        }
    }
}
