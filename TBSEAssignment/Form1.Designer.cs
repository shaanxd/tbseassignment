﻿namespace TBSEAssignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripFileDropDown = new System.Windows.Forms.ToolStripDropDownButton();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripNewButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripOpenButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripsSaveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSaveAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripCopyButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripPasteButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.fileList = new System.Windows.Forms.ListView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.addCommonButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteContentbutton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.findAndReplaceButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.txtFormattingBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.encryptDropDown = new System.Windows.Forms.ToolStripDropDownButton();
            this.encryptSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decryptSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encryptAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decryptAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.boldButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.italicButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.underlineButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.copyDropDown = new System.Windows.Forms.ToolStripDropDownButton();
            this.copySelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteContentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFileDropDown});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(991, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripFileDropDown
            // 
            this.toolStripFileDropDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripFileDropDown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.toolStripFileDropDown.Image = ((System.Drawing.Image)(resources.GetObject("toolStripFileDropDown.Image")));
            this.toolStripFileDropDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripFileDropDown.Name = "toolStripFileDropDown";
            this.toolStripFileDropDown.Size = new System.Drawing.Size(38, 22);
            this.toolStripFileDropDown.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripNewButton,
            this.toolStripSeparator1,
            this.toolStripOpenButton,
            this.toolStripSeparator2,
            this.toolStripsSaveButton,
            this.toolStripSeparator4,
            this.toolStripSaveAllButton,
            this.toolStripSeparator13,
            this.toolStripCopyButton,
            this.toolStripSeparator14,
            this.toolStripPasteButton,
            this.toolStripSeparator15});
            this.toolStrip2.Location = new System.Drawing.Point(0, 25);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(991, 73);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripNewButton
            // 
            this.toolStripNewButton.AutoSize = false;
            this.toolStripNewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripNewButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripNewButton.Image")));
            this.toolStripNewButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripNewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripNewButton.Name = "toolStripNewButton";
            this.toolStripNewButton.Size = new System.Drawing.Size(70, 70);
            this.toolStripNewButton.Text = "New Document";
            this.toolStripNewButton.ToolTipText = "New Document";
            this.toolStripNewButton.Click += new System.EventHandler(this.toolStripNewButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 73);
            // 
            // toolStripOpenButton
            // 
            this.toolStripOpenButton.AutoSize = false;
            this.toolStripOpenButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripOpenButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOpenButton.Image")));
            this.toolStripOpenButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripOpenButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripOpenButton.Name = "toolStripOpenButton";
            this.toolStripOpenButton.Size = new System.Drawing.Size(70, 70);
            this.toolStripOpenButton.Text = "Open Document";
            this.toolStripOpenButton.ToolTipText = "Open Document";
            this.toolStripOpenButton.Click += new System.EventHandler(this.toolStripOpenButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 73);
            // 
            // toolStripsSaveButton
            // 
            this.toolStripsSaveButton.AutoSize = false;
            this.toolStripsSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripsSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripsSaveButton.Image")));
            this.toolStripsSaveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripsSaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripsSaveButton.Name = "toolStripsSaveButton";
            this.toolStripsSaveButton.Size = new System.Drawing.Size(70, 70);
            this.toolStripsSaveButton.Text = "Save";
            this.toolStripsSaveButton.ToolTipText = "Save";
            this.toolStripsSaveButton.Click += new System.EventHandler(this.toolStripsSaveButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 73);
            // 
            // toolStripSaveAllButton
            // 
            this.toolStripSaveAllButton.AutoSize = false;
            this.toolStripSaveAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveAllButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveAllButton.Image")));
            this.toolStripSaveAllButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSaveAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSaveAllButton.Name = "toolStripSaveAllButton";
            this.toolStripSaveAllButton.Size = new System.Drawing.Size(70, 70);
            this.toolStripSaveAllButton.Text = "Save All";
            this.toolStripSaveAllButton.ToolTipText = "Save All";
            this.toolStripSaveAllButton.Click += new System.EventHandler(this.toolStripSaveAllButton_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 73);
            // 
            // toolStripCopyButton
            // 
            this.toolStripCopyButton.AutoSize = false;
            this.toolStripCopyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripCopyButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCopyButton.Image")));
            this.toolStripCopyButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripCopyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCopyButton.Name = "toolStripCopyButton";
            this.toolStripCopyButton.Size = new System.Drawing.Size(70, 70);
            this.toolStripCopyButton.Text = "Copy";
            this.toolStripCopyButton.Click += new System.EventHandler(this.toolStripCopyButton_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 73);
            // 
            // toolStripPasteButton
            // 
            this.toolStripPasteButton.AutoSize = false;
            this.toolStripPasteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripPasteButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripPasteButton.Image")));
            this.toolStripPasteButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripPasteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripPasteButton.Name = "toolStripPasteButton";
            this.toolStripPasteButton.Size = new System.Drawing.Size(70, 70);
            this.toolStripPasteButton.Text = "Paste";
            this.toolStripPasteButton.Click += new System.EventHandler(this.toolStripPasteButton_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 73);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 534);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(991, 30);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.fileList);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 131);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(162, 403);
            this.panel2.TabIndex = 4;
            // 
            // fileList
            // 
            this.fileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileList.Location = new System.Drawing.Point(10, 10);
            this.fileList.Name = "fileList";
            this.fileList.Size = new System.Drawing.Size(138, 379);
            this.fileList.TabIndex = 0;
            this.fileList.TileSize = new System.Drawing.Size(130, 40);
            this.fileList.UseCompatibleStateImageBehavior = false;
            this.fileList.View = System.Windows.Forms.View.Tile;
            this.fileList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.fileList_MouseDoubleClick);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.tabControl);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(162, 131);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(829, 403);
            this.panel3.TabIndex = 5;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl.Location = new System.Drawing.Point(10, 10);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(805, 379);
            this.tabControl.TabIndex = 0;
            this.tabControl.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl_DrawItem);
            this.tabControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tabControl_MouseDown);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCommonButton,
            this.toolStripSeparator3,
            this.deleteContentbutton,
            this.toolStripSeparator5,
            this.findAndReplaceButton,
            this.toolStripSeparator11,
            this.txtFormattingBtn,
            this.toolStripSeparator6,
            this.encryptDropDown,
            this.toolStripSeparator7,
            this.boldButton,
            this.toolStripSeparator9,
            this.italicButton,
            this.toolStripSeparator8,
            this.underlineButton,
            this.toolStripSeparator10,
            this.copyDropDown,
            this.toolStripSeparator12});
            this.toolStrip3.Location = new System.Drawing.Point(0, 98);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(991, 33);
            this.toolStrip3.TabIndex = 3;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // addCommonButton
            // 
            this.addCommonButton.AutoSize = false;
            this.addCommonButton.Image = ((System.Drawing.Image)(resources.GetObject("addCommonButton.Image")));
            this.addCommonButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addCommonButton.Name = "addCommonButton";
            this.addCommonButton.Padding = new System.Windows.Forms.Padding(5);
            this.addCommonButton.Size = new System.Drawing.Size(110, 30);
            this.addCommonButton.Text = "Add Content";
            this.addCommonButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addCommonButton.Click += new System.EventHandler(this.addCommonButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 33);
            // 
            // deleteContentbutton
            // 
            this.deleteContentbutton.AutoSize = false;
            this.deleteContentbutton.Image = ((System.Drawing.Image)(resources.GetObject("deleteContentbutton.Image")));
            this.deleteContentbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteContentbutton.Name = "deleteContentbutton";
            this.deleteContentbutton.Padding = new System.Windows.Forms.Padding(5);
            this.deleteContentbutton.Size = new System.Drawing.Size(110, 30);
            this.deleteContentbutton.Text = "Delete Content";
            this.deleteContentbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteContentbutton.ToolTipText = "Delete Content";
            this.deleteContentbutton.Click += new System.EventHandler(this.deleteContentbutton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 33);
            // 
            // findAndReplaceButton
            // 
            this.findAndReplaceButton.Image = ((System.Drawing.Image)(resources.GetObject("findAndReplaceButton.Image")));
            this.findAndReplaceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.findAndReplaceButton.Name = "findAndReplaceButton";
            this.findAndReplaceButton.Padding = new System.Windows.Forms.Padding(5);
            this.findAndReplaceButton.Size = new System.Drawing.Size(127, 30);
            this.findAndReplaceButton.Text = "Find and Replace";
            this.findAndReplaceButton.ToolTipText = "Find and Replace";
            this.findAndReplaceButton.Click += new System.EventHandler(this.findAndReplaceButton_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 33);
            // 
            // txtFormattingBtn
            // 
            this.txtFormattingBtn.Image = ((System.Drawing.Image)(resources.GetObject("txtFormattingBtn.Image")));
            this.txtFormattingBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.txtFormattingBtn.Name = "txtFormattingBtn";
            this.txtFormattingBtn.Padding = new System.Windows.Forms.Padding(5);
            this.txtFormattingBtn.Size = new System.Drawing.Size(120, 30);
            this.txtFormattingBtn.Text = "Text Formatting";
            this.txtFormattingBtn.Click += new System.EventHandler(this.txtFormattingBtn_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 33);
            // 
            // encryptDropDown
            // 
            this.encryptDropDown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.encryptSelectedToolStripMenuItem,
            this.decryptSelectedToolStripMenuItem,
            this.encryptAllToolStripMenuItem,
            this.decryptAllToolStripMenuItem});
            this.encryptDropDown.Image = ((System.Drawing.Image)(resources.GetObject("encryptDropDown.Image")));
            this.encryptDropDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.encryptDropDown.Name = "encryptDropDown";
            this.encryptDropDown.Padding = new System.Windows.Forms.Padding(5);
            this.encryptDropDown.Size = new System.Drawing.Size(148, 30);
            this.encryptDropDown.Text = "Encryption Options";
            this.encryptDropDown.ToolTipText = "Encryption Options";
            // 
            // encryptSelectedToolStripMenuItem
            // 
            this.encryptSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("encryptSelectedToolStripMenuItem.Image")));
            this.encryptSelectedToolStripMenuItem.Name = "encryptSelectedToolStripMenuItem";
            this.encryptSelectedToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.encryptSelectedToolStripMenuItem.Text = "Encrypt Selected";
            this.encryptSelectedToolStripMenuItem.Click += new System.EventHandler(this.encryptSelectedToolStripMenuItem_Click);
            // 
            // decryptSelectedToolStripMenuItem
            // 
            this.decryptSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("decryptSelectedToolStripMenuItem.Image")));
            this.decryptSelectedToolStripMenuItem.Name = "decryptSelectedToolStripMenuItem";
            this.decryptSelectedToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.decryptSelectedToolStripMenuItem.Text = "Decrypt Selected";
            this.decryptSelectedToolStripMenuItem.Click += new System.EventHandler(this.decryptSelectedToolStripMenuItem_Click);
            // 
            // encryptAllToolStripMenuItem
            // 
            this.encryptAllToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("encryptAllToolStripMenuItem.Image")));
            this.encryptAllToolStripMenuItem.Name = "encryptAllToolStripMenuItem";
            this.encryptAllToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.encryptAllToolStripMenuItem.Text = "Encrypt All";
            this.encryptAllToolStripMenuItem.Click += new System.EventHandler(this.encryptAllToolStripMenuItem_Click);
            // 
            // decryptAllToolStripMenuItem
            // 
            this.decryptAllToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("decryptAllToolStripMenuItem.Image")));
            this.decryptAllToolStripMenuItem.Name = "decryptAllToolStripMenuItem";
            this.decryptAllToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.decryptAllToolStripMenuItem.Text = "Decrypt All";
            this.decryptAllToolStripMenuItem.Click += new System.EventHandler(this.decryptAllToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 33);
            // 
            // boldButton
            // 
            this.boldButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.boldButton.Image = ((System.Drawing.Image)(resources.GetObject("boldButton.Image")));
            this.boldButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.boldButton.Name = "boldButton";
            this.boldButton.Padding = new System.Windows.Forms.Padding(5);
            this.boldButton.Size = new System.Drawing.Size(30, 30);
            this.boldButton.Text = "Bold";
            this.boldButton.ToolTipText = "Bold";
            this.boldButton.Click += new System.EventHandler(this.boldButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 33);
            // 
            // italicButton
            // 
            this.italicButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.italicButton.Image = ((System.Drawing.Image)(resources.GetObject("italicButton.Image")));
            this.italicButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.italicButton.Name = "italicButton";
            this.italicButton.Padding = new System.Windows.Forms.Padding(5);
            this.italicButton.Size = new System.Drawing.Size(30, 30);
            this.italicButton.Text = "Italic";
            this.italicButton.ToolTipText = "Italic";
            this.italicButton.Click += new System.EventHandler(this.italicButton_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 33);
            // 
            // underlineButton
            // 
            this.underlineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.underlineButton.Image = ((System.Drawing.Image)(resources.GetObject("underlineButton.Image")));
            this.underlineButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.underlineButton.Name = "underlineButton";
            this.underlineButton.Padding = new System.Windows.Forms.Padding(5);
            this.underlineButton.Size = new System.Drawing.Size(30, 30);
            this.underlineButton.Text = "Underline";
            this.underlineButton.Click += new System.EventHandler(this.underlineButton_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 33);
            // 
            // copyDropDown
            // 
            this.copyDropDown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copySelectedToolStripMenuItem,
            this.pasteContentToolStripMenuItem});
            this.copyDropDown.Image = ((System.Drawing.Image)(resources.GetObject("copyDropDown.Image")));
            this.copyDropDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyDropDown.Name = "copyDropDown";
            this.copyDropDown.Padding = new System.Windows.Forms.Padding(5);
            this.copyDropDown.Size = new System.Drawing.Size(119, 30);
            this.copyDropDown.Text = "Copy Options";
            this.copyDropDown.ToolTipText = "Copy Options";
            // 
            // copySelectedToolStripMenuItem
            // 
            this.copySelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copySelectedToolStripMenuItem.Image")));
            this.copySelectedToolStripMenuItem.Name = "copySelectedToolStripMenuItem";
            this.copySelectedToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.copySelectedToolStripMenuItem.Text = "Copy Document";
            this.copySelectedToolStripMenuItem.Click += new System.EventHandler(this.copySelectedToolStripMenuItem_Click);
            // 
            // pasteContentToolStripMenuItem
            // 
            this.pasteContentToolStripMenuItem.Enabled = false;
            this.pasteContentToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteContentToolStripMenuItem.Image")));
            this.pasteContentToolStripMenuItem.Name = "pasteContentToolStripMenuItem";
            this.pasteContentToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.pasteContentToolStripMenuItem.Text = "Paste Document";
            this.pasteContentToolStripMenuItem.Click += new System.EventHandler(this.pasteContentToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 33);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "icons8-microsoft-word-50.png");
            this.imageList.Images.SetKeyName(1, "icons8-microsoft-onenote-50.png");
            this.imageList.Images.SetKeyName(2, "icons8-microsoft-excel-50.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 564);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripOpenButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStripButton toolStripsSaveButton;
        private System.Windows.Forms.ToolStripButton toolStripSaveAllButton;
        private System.Windows.Forms.ToolStripButton toolStripNewButton;
        private System.Windows.Forms.ToolStripDropDownButton toolStripFileDropDown;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton addCommonButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton deleteContentbutton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton txtFormattingBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripDropDownButton encryptDropDown;
        private System.Windows.Forms.ToolStripMenuItem encryptSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decryptSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encryptAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decryptAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton boldButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton italicButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton underlineButton;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripDropDownButton copyDropDown;
        private System.Windows.Forms.ToolStripMenuItem copySelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteContentToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton findAndReplaceButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripButton toolStripCopyButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripPasteButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ListView fileList;
        private System.Windows.Forms.ImageList imageList;
    }
}

