﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace TBSEAssignment
{
    public partial class Form1 : Form , IMainFrm
    {
        AesManaged mAesMng;

        string[,] mClipboardArray;
        string mClipboardString;

        string mFilesListPath = "history.txt";
        string mKeysPath = "keys.txt";

        List<string> mFileList = new List<string>();

        enum TabType
        {
            TEXT,
            WORD,
            EXCEL
        }

        public Form1()
        {
            InitializeComponent();
            mInitializeAes();
            mInitializeFiles();
            mSetListView();
            TaskScheduler.UnobservedTaskException += new EventHandler<UnobservedTaskExceptionEventArgs>(TaskUnobservedException_Handler);

            System.Timers.Timer mSaveTimer = new System.Timers.Timer(60000);
            mSaveTimer.Elapsed += mAutoSaveFiles;
            mSaveTimer.Start();
        }

        static void TaskUnobservedException_Handler(object sender, UnobservedTaskExceptionEventArgs e)
        {
            MessageBox.Show("An Error Occured. Please try again.");
            e.SetObserved();
        }

        private void mInitializeAes()
        {
            try
            {
                if (mAesMng == null)
                {
                    mAesMng = new AesManaged();
                }
                if (File.Exists(mKeysPath))
                {
                    var mLines = File.ReadAllLines(mKeysPath);
                    mAesMng.Key = Convert.FromBase64String(mLines[0]);
                    mAesMng.IV = Convert.FromBase64String(mLines[1]);
                }
                else
                {
                    string xKey = Convert.ToBase64String(mAesMng.Key) + "\n" + Convert.ToBase64String(mAesMng.IV);
                    File.WriteAllLines(mKeysPath, xKey.Split('\n'));
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void mInitializeFiles()
        {
            try
            {
                if (File.Exists(mFilesListPath))
                {
                    var mLines = File.ReadAllLines("history.txt");
                    foreach (string mString in mLines)
                    {
                        mFileList.Add(mString);
                    }
                }
                else
                {
                    File.Create("history.txt");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void mSetListView()
        {
            try
            {
                fileList.Items.Clear();
                fileList.LargeImageList = imageList;
                foreach (string mLine in mFileList)
                {
                    var mListViewItem = new ListViewItem();
                    mListViewItem.Text = Path.GetFileName(mLine);
                    mListViewItem.Tag = mLine;
                    switch (Path.GetExtension(mLine))
                    {
                        case ".txt":
                            mListViewItem.ImageIndex = 1;
                            break;
                        case ".docx":
                            mListViewItem.ImageIndex = 0;
                            break;
                        case ".xlsx":
                            mListViewItem.ImageIndex = 2;
                            break;
                    }
                    fileList.Items.Add(mListViewItem);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void mWriteFiles()
        {
            File.WriteAllLines(mFilesListPath, mFileList);
        }

        private void toolStripOpenButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog mDialog = new OpenFileDialog();
                mDialog.Multiselect = true;
                mDialog.Filter = "Text Files (.txt)|*.txt" +
                    "|Word Documents (.docx)|*.docx" +
                    "|Excel Workbooks (.xlsx)|*.xlsx";

                if (mDialog.ShowDialog() == DialogResult.OK)
                {
                    foreach (string mFileName in mDialog.FileNames)
                    {
                        int mResult = mCheckFileOpen(mFileName);
                        if (mResult == -1)
                        {
                            mNewFile(mFileName);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void toolStripsSaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTab = (ITab)tabControl.SelectedTab;
                if (mTab != null)
                {
                    Task mTask = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            mTab.mSaveFile();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });
                }
                else
                {
                    MessageBox.Show("No Documents Selected");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void toolStripSaveAllButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (tabControl.TabPages.Count != 0)
                {
                    foreach (ITab mTabPage in tabControl.TabPages)
                    {
                        Task mTask = Task.Factory.StartNew(() =>
                        {
                            mTabPage.mSaveFile();
                        });
                    }
                }
                else
                {
                    MessageBox.Show("No Documents Open.");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void toolStripNewButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog mDialog = new SaveFileDialog();
                mDialog.Title = "Select Location";
                mDialog.Filter = "Text Files (.txt)|*.txt|Word Documents (.docx)|*.docx|Excel Workbooks (.xlsx)|*.xlsx";
                if (mDialog.ShowDialog() == DialogResult.OK)
                {
                    mNewFile(mDialog.FileName);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mNewFile(String mFilePath)
        {
            TabPage mTabPage = null;
            Task mTask = Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!mFileList.Contains(mFilePath))
                    {
                        mFileList.Add(mFilePath);
                    }
                    switch (Path.GetExtension(mFilePath))
                    {
                        case ".txt":
                            mTabPage = new TextTabPage(mFilePath, TabType.TEXT);
                            break;
                        case ".docx":
                            mTabPage = new WordTabPage(mFilePath, TabType.WORD);
                            break;
                        case ".xlsx":
                            mTabPage = new ExcelTabPage(mFilePath, TabType.EXCEL);
                            break;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            });
            Task mContinueTask = mTask.ContinueWith((mPrevTask) =>
            {
                try
                {
                    tabControl.Controls.Add(mTabPage);
                    tabControl.SelectedTab = mTabPage;
                    mSetListView();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }, 
            TaskScheduler.FromCurrentSynchronizationContext()
            );            
        }

        public int mCheckFileOpen(String mFilePath)
        {
            foreach(ITab mTabPage in tabControl.TabPages)
            {
                if (mFilePath.Equals(mTabPage.mGetFilePath()))
                {
                    return tabControl.TabPages.IndexOf((TabPage)mTabPage);
                }
            }
            return -1;
        }

        private void tabControl_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                TabPage mTabPage = tabControl.TabPages[e.Index];
                Rectangle mTabRect = tabControl.GetTabRect(e.Index);
                mTabRect.Inflate(-2, -2);

                //e.Graphics.FillRectangle(new SolidBrush(Color.White), mTabRect);

                Bitmap mCloseImage = new Bitmap(Properties.Resources.close);
                e.Graphics.DrawImage(mCloseImage, (mTabRect.Right - mCloseImage.Width), mTabRect.Top + (mTabRect.Height - mCloseImage.Height) / 2);
                TextRenderer.DrawText(e.Graphics, mTabPage.Text, mTabPage.Font, mTabRect, mTabPage.ForeColor, TextFormatFlags.Left);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void tabControl_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                for (int i = 0; i < tabControl.TabPages.Count; i++)
                {
                    Rectangle mTabRect = tabControl.GetTabRect(i);
                    mTabRect.Inflate(-2, -2);

                    Bitmap mCloseImage = new Bitmap(Properties.Resources.close);
                    Rectangle mImageRect = new Rectangle(
                        (mTabRect.Right - mCloseImage.Width),
                        mTabRect.Top + (mTabRect.Height - mCloseImage.Height) / 2,
                        mCloseImage.Width, mCloseImage.Height);
                    if (mImageRect.Contains(e.Location))
                    {
                        int mValue = i;
                        Task mTask = Task.Factory.StartNew(() =>
                        {
                            try
                            {
                                ((ITab)tabControl.TabPages[mValue]).mCloseFile();
                            }
                            catch(Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        });
                        Task mSecondTask = mTask.ContinueWith((mPrevious) =>
                        {
                            try
                            {
                                GC.Collect();
                            }
                            catch(Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void mAutoSaveFiles(object sender, ElapsedEventArgs e)
        {
            foreach (ITab mTabPage in tabControl.TabPages)
            {
                if (mTabPage.mGetFileState())
                {
                    Task mTask = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            mTabPage.mSaveFile();
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });
                }
            }
        }

        private void addCommonButton_Click(object sender, EventArgs e)
        {
            AddCommonFrm mFrm = new AddCommonFrm(this);
            mFrm.ShowDialog();
        }

        public void mAddToAllText(string mText)
        {
            try
            {
                Parallel.ForEach(tabControl.TabPages.Cast<ITab>(), mTabPage =>
                {
                    if (mTabPage.mGetTabType().Equals(TabType.TEXT) 
                    || mTabPage.mGetTabType().Equals(TabType.WORD))
                    {
                        Task mTask = Task.Factory.StartNew(() =>
                        {
                            mTabPage.mAddText(mText, 0, 0);
                        });
                    }
                });
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mAddToAllExcel(string mText, int mColumnCount, int mRowCount)
        {
            try
            {
                Parallel.ForEach(tabControl.TabPages.Cast<ITab>(), mTabPage =>
                {
                    if (mTabPage.mGetTabType().Equals(TabType.EXCEL))
                    {
                        Task mTask = Task.Factory.StartNew(() =>
                        {
                            mTabPage.mAddText(mText, mColumnCount, mRowCount);
                        });
                    }
                });
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mAddToExcel(string mText, int mColumnCount, int mRowCount)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if ((mTabPage != null))
                {
                    if (mTabPage.mGetTabType().Equals(TabType.EXCEL))
                    {
                        Task mTask = Task.Factory.StartNew(() =>
                        {
                            mTabPage.mAddText(mText, mColumnCount, mRowCount);
                        });
                    }
                    else
                    {
                        MessageBox.Show("Please select an Excel Document");
                    }
                }
                else
                {
                    MessageBox.Show("No Document Selected");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void deleteContentbutton_Click(object sender, EventArgs e)
        {
            DeleteContentFrm mFrm = new DeleteContentFrm(this);
            mFrm.ShowDialog();
        }

        public void mDeleteText(string mText, bool mIgnoreCase)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if (mTabPage != null)
                {
                    mTabPage.mFindAndReplace(mText, mIgnoreCase, "");
                }
                else
                {
                    MessageBox.Show("No Tabs Open");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mDeleteAll(string mText, bool mIgnoreCase)
        {
            try
            {
                foreach (ITab mTabPage in tabControl.TabPages)
                {
                    mTabPage.mFindAndReplace(mText, mIgnoreCase, "");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void txtFormattingBtn_Click(object sender, EventArgs e)
        {
            TextFormattingFrm mFrm = new TextFormattingFrm(this);
            mFrm.ShowDialog();
        }

        public void mApplyFormatting(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if (mTabPage != null)
                {
                    mTabPage.mFormatText(mText, mIgnoreCase, mIsBold, mIsItalic, mIsUnderline);
                }
                else
                {
                    MessageBox.Show("No Tabs Open");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mApplyFormattingToAll(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline)
        {
            try
            {
                foreach (ITab mTabPage in tabControl.TabPages)
                {
                    mTabPage.mFormatText(mText, mIgnoreCase, mIsBold, mIsItalic, mIsUnderline);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void encryptSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if (mTabPage != null)
                {
                    mTabPage.mEncrypt(mAesMng.Key, mAesMng.IV);
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void decryptSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if (mTabPage != null)
                {
                    mTabPage.mDecrypt(mAesMng.Key, mAesMng.IV);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void encryptAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ITab mTabPage in tabControl.TabPages)
                {
                    mTabPage.mEncrypt(mAesMng.Key, mAesMng.IV);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void decryptAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ITab mTabPage in tabControl.TabPages)
            {
                mTabPage.mDecrypt(mAesMng.Key, mAesMng.IV);
            }
        }

        private void boldButton_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;

                if (mTabPage != null)
                {
                    mTabPage.mAddFormatting(FontStyle.Bold);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void italicButton_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;

                if (mTabPage != null)
                {
                    mTabPage.mAddFormatting(FontStyle.Italic);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void underlineButton_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;

                if (mTabPage != null)
                {
                    mTabPage.mAddFormatting(FontStyle.Underline);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void copySelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;

                if (mTabPage != null)
                {
                    mClipboardArray = mTabPage.mCopyText();
                    pasteContentToolStripMenuItem.Enabled = true;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void pasteContentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mClipboardArray != null)
                {
                    ITab mTabPage = (ITab)tabControl.SelectedTab;

                    if (mTabPage != null)
                    {
                        mTabPage.mPasteText(mClipboardArray);
                    }
                };
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mFindAndReplace(string mText, bool mIgnoreCase, string mReplacingText)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;

                if (mTabPage != null)
                {
                    mTabPage.mFindAndReplace(mText, mIgnoreCase, mReplacingText);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void mFindAndReplaceAll(string mText, bool mIgnoreCase, string mReplacingText)
        {
            try
            {
                foreach (ITab mTabPage in tabControl.TabPages)
                {
                    mTabPage.mFindAndReplace(mText, mIgnoreCase, mReplacingText);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void findAndReplaceButton_Click(object sender, EventArgs e)
        {
            FindReplaceFrm mFrm = new FindReplaceFrm(this);
            mFrm.ShowDialog();
        }

        private void toolStripCopyButton_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if (mTabPage != null)
                {
                    mClipboardString = mTabPage.mCopySingle();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void toolStripPasteButton_Click(object sender, EventArgs e)
        {
            try
            {
                ITab mTabPage = (ITab)tabControl.SelectedTab;
                if (mTabPage != null)
                {
                    mTabPage.mPasteSingle(mClipboardString);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void mCloseApplication()
        {
            try
            {
                List<Task> mTaskList = new List<Task>();

                foreach (ITab mTabPage in tabControl.TabPages)
                {
                    int mValue = tabControl.TabPages.IndexOf((TabPage)mTabPage);
                    Task mTask = Task.Factory.StartNew(() =>
                    {
                        ((ITab)tabControl.TabPages[mValue]).mCloseFile();
                    });
                    mTaskList.Add(mTask);
                }
                mWriteFiles();
                Task.WaitAll(mTaskList.ToArray());
                GC.Collect();
                Application.Exit();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                GC.Collect();
                Application.Exit();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Task mTask = Task.Factory.StartNew(() =>
            {
                mCloseApplication();
            });
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Task mTask = Task.Factory.StartNew(() =>
                {
                    mCloseApplication();
                });
            }
            else
            {
                return;
            }
        }

        private void fileList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (fileList.SelectedItems.Count == 1)
                {
                    mNewFile(fileList.SelectedItems[0].Tag as string);
                }
                else if (fileList.SelectedItems.Count > 1)
                {
                    foreach (ListViewItem mItem in fileList.SelectedItems)
                    {
                        mNewFile(mItem.Tag as string);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
