﻿namespace TBSEAssignment
{
    partial class AddCommonFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contentTxtBox = new System.Windows.Forms.RichTextBox();
            this.submitBtn = new System.Windows.Forms.Button();
            this.fileTypeGrpBox = new System.Windows.Forms.GroupBox();
            this.txtRadioBtn = new System.Windows.Forms.RadioButton();
            this.excelRadioBtn = new System.Windows.Forms.RadioButton();
            this.excelGrpBox = new System.Windows.Forms.GroupBox();
            this.columnNumber = new System.Windows.Forms.NumericUpDown();
            this.rowCount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.currentRadioBtn = new System.Windows.Forms.RadioButton();
            this.allRadioBtn = new System.Windows.Forms.RadioButton();
            this.fileTypeGrpBox.SuspendLayout();
            this.excelGrpBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowCount)).BeginInit();
            this.SuspendLayout();
            // 
            // contentTxtBox
            // 
            this.contentTxtBox.Location = new System.Drawing.Point(12, 73);
            this.contentTxtBox.Name = "contentTxtBox";
            this.contentTxtBox.Size = new System.Drawing.Size(260, 76);
            this.contentTxtBox.TabIndex = 0;
            this.contentTxtBox.Text = "";
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(12, 261);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(260, 23);
            this.submitBtn.TabIndex = 1;
            this.submitBtn.Text = "Add";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // fileTypeGrpBox
            // 
            this.fileTypeGrpBox.Controls.Add(this.excelRadioBtn);
            this.fileTypeGrpBox.Controls.Add(this.txtRadioBtn);
            this.fileTypeGrpBox.Location = new System.Drawing.Point(12, 12);
            this.fileTypeGrpBox.Name = "fileTypeGrpBox";
            this.fileTypeGrpBox.Size = new System.Drawing.Size(260, 55);
            this.fileTypeGrpBox.TabIndex = 2;
            this.fileTypeGrpBox.TabStop = false;
            this.fileTypeGrpBox.Text = "File Type";
            // 
            // txtRadioBtn
            // 
            this.txtRadioBtn.AutoSize = true;
            this.txtRadioBtn.Checked = true;
            this.txtRadioBtn.Location = new System.Drawing.Point(6, 29);
            this.txtRadioBtn.Name = "txtRadioBtn";
            this.txtRadioBtn.Size = new System.Drawing.Size(100, 17);
            this.txtRadioBtn.TabIndex = 0;
            this.txtRadioBtn.TabStop = true;
            this.txtRadioBtn.Text = "Text/Document";
            this.txtRadioBtn.UseVisualStyleBackColor = true;
            // 
            // excelRadioBtn
            // 
            this.excelRadioBtn.AutoSize = true;
            this.excelRadioBtn.Location = new System.Drawing.Point(112, 29);
            this.excelRadioBtn.Name = "excelRadioBtn";
            this.excelRadioBtn.Size = new System.Drawing.Size(51, 17);
            this.excelRadioBtn.TabIndex = 1;
            this.excelRadioBtn.Text = "Excel";
            this.excelRadioBtn.UseVisualStyleBackColor = true;
            this.excelRadioBtn.CheckedChanged += new System.EventHandler(this.excelRadioBtn_CheckedChanged);
            // 
            // excelGrpBox
            // 
            this.excelGrpBox.Controls.Add(this.currentRadioBtn);
            this.excelGrpBox.Controls.Add(this.allRadioBtn);
            this.excelGrpBox.Controls.Add(this.label2);
            this.excelGrpBox.Controls.Add(this.label1);
            this.excelGrpBox.Controls.Add(this.rowCount);
            this.excelGrpBox.Controls.Add(this.columnNumber);
            this.excelGrpBox.Enabled = false;
            this.excelGrpBox.Location = new System.Drawing.Point(12, 155);
            this.excelGrpBox.Name = "excelGrpBox";
            this.excelGrpBox.Size = new System.Drawing.Size(260, 100);
            this.excelGrpBox.TabIndex = 3;
            this.excelGrpBox.TabStop = false;
            this.excelGrpBox.Text = "Excel Operations";
            // 
            // columnNumber
            // 
            this.columnNumber.Location = new System.Drawing.Point(96, 46);
            this.columnNumber.Name = "columnNumber";
            this.columnNumber.Size = new System.Drawing.Size(120, 20);
            this.columnNumber.TabIndex = 4;
            // 
            // rowCount
            // 
            this.rowCount.Location = new System.Drawing.Point(96, 72);
            this.rowCount.Name = "rowCount";
            this.rowCount.Size = new System.Drawing.Size(120, 20);
            this.rowCount.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Column Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Row Count";
            // 
            // currentRadioBtn
            // 
            this.currentRadioBtn.AutoSize = true;
            this.currentRadioBtn.Checked = true;
            this.currentRadioBtn.Location = new System.Drawing.Point(6, 23);
            this.currentRadioBtn.Name = "currentRadioBtn";
            this.currentRadioBtn.Size = new System.Drawing.Size(111, 17);
            this.currentRadioBtn.TabIndex = 4;
            this.currentRadioBtn.TabStop = true;
            this.currentRadioBtn.Text = "Current Document";
            this.currentRadioBtn.UseVisualStyleBackColor = true;
            // 
            // allRadioBtn
            // 
            this.allRadioBtn.AutoSize = true;
            this.allRadioBtn.Location = new System.Drawing.Point(123, 23);
            this.allRadioBtn.Name = "allRadioBtn";
            this.allRadioBtn.Size = new System.Drawing.Size(93, 17);
            this.allRadioBtn.TabIndex = 5;
            this.allRadioBtn.Text = "All Documents";
            this.allRadioBtn.UseVisualStyleBackColor = true;
            // 
            // AddCommonFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 294);
            this.Controls.Add(this.excelGrpBox);
            this.Controls.Add(this.fileTypeGrpBox);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.contentTxtBox);
            this.Name = "AddCommonFrm";
            this.Text = "Add Content";
            this.fileTypeGrpBox.ResumeLayout(false);
            this.fileTypeGrpBox.PerformLayout();
            this.excelGrpBox.ResumeLayout(false);
            this.excelGrpBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox contentTxtBox;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.GroupBox fileTypeGrpBox;
        private System.Windows.Forms.RadioButton excelRadioBtn;
        private System.Windows.Forms.RadioButton txtRadioBtn;
        private System.Windows.Forms.GroupBox excelGrpBox;
        private System.Windows.Forms.RadioButton currentRadioBtn;
        private System.Windows.Forms.RadioButton allRadioBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown rowCount;
        private System.Windows.Forms.NumericUpDown columnNumber;
    }
}