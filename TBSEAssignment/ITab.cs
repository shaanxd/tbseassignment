﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSEAssignment
{
    interface ITab
    {
        void mReadFile();
        void mCreateFile();
        void mSaveFile();
        void mCloseFile();
        string mGetFilePath();
        bool mGetFileState();
        Enum mGetTabType();
        void mAddText(string mText, int mColumnNumber, int mRowCount);
        void mFindAndReplace(string mText, bool mIgnoreCase, string mReplacingText);
        void mFormatText(string mText, bool mIgnoreCase, bool mIsBold, bool mIsItalic, bool mIsUnderline);
        void mEncrypt(byte[] mKey, byte[] mIV);
        void mDecrypt(byte[] mKey, byte[] mIV);
        void mAddFormatting(FontStyle mStyle);
        string[,] mCopyText();
        void mPasteText(string[,] mStringArray);
        string mCopySingle();
        void mPasteSingle(string mCopyString);
    }
}
