﻿namespace TBSEAssignment
{
    partial class TextFormattingFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.currentRadioBtn = new System.Windows.Forms.RadioButton();
            this.allRadioBtn = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.formattingTxt = new System.Windows.Forms.RichTextBox();
            this.underlineBox = new System.Windows.Forms.CheckBox();
            this.italicBox = new System.Windows.Forms.CheckBox();
            this.boldBox = new System.Windows.Forms.CheckBox();
            this.ignoreBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.submitBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.allRadioBtn);
            this.groupBox1.Controls.Add(this.currentRadioBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 44);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Files";
            // 
            // currentRadioBtn
            // 
            this.currentRadioBtn.AutoSize = true;
            this.currentRadioBtn.Checked = true;
            this.currentRadioBtn.Location = new System.Drawing.Point(6, 16);
            this.currentRadioBtn.Name = "currentRadioBtn";
            this.currentRadioBtn.Size = new System.Drawing.Size(111, 17);
            this.currentRadioBtn.TabIndex = 0;
            this.currentRadioBtn.TabStop = true;
            this.currentRadioBtn.Text = "Current Document";
            this.currentRadioBtn.UseVisualStyleBackColor = true;
            // 
            // allRadioBtn
            // 
            this.allRadioBtn.AutoSize = true;
            this.allRadioBtn.Location = new System.Drawing.Point(123, 16);
            this.allRadioBtn.Name = "allRadioBtn";
            this.allRadioBtn.Size = new System.Drawing.Size(93, 17);
            this.allRadioBtn.TabIndex = 1;
            this.allRadioBtn.TabStop = true;
            this.allRadioBtn.Text = "All Documents";
            this.allRadioBtn.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ignoreBox);
            this.groupBox2.Controls.Add(this.formattingTxt);
            this.groupBox2.Location = new System.Drawing.Point(12, 62);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 92);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Text";
            // 
            // formattingTxt
            // 
            this.formattingTxt.Location = new System.Drawing.Point(6, 19);
            this.formattingTxt.Name = "formattingTxt";
            this.formattingTxt.Size = new System.Drawing.Size(248, 47);
            this.formattingTxt.TabIndex = 0;
            this.formattingTxt.Text = "";
            // 
            // underlineBox
            // 
            this.underlineBox.AutoSize = true;
            this.underlineBox.Location = new System.Drawing.Point(113, 24);
            this.underlineBox.Name = "underlineBox";
            this.underlineBox.Size = new System.Drawing.Size(71, 17);
            this.underlineBox.TabIndex = 1;
            this.underlineBox.Text = "Underline";
            this.underlineBox.UseVisualStyleBackColor = true;
            // 
            // italicBox
            // 
            this.italicBox.AutoSize = true;
            this.italicBox.Location = new System.Drawing.Point(59, 24);
            this.italicBox.Name = "italicBox";
            this.italicBox.Size = new System.Drawing.Size(48, 17);
            this.italicBox.TabIndex = 2;
            this.italicBox.Text = "Italic";
            this.italicBox.UseVisualStyleBackColor = true;
            // 
            // boldBox
            // 
            this.boldBox.AutoSize = true;
            this.boldBox.Location = new System.Drawing.Point(6, 24);
            this.boldBox.Name = "boldBox";
            this.boldBox.Size = new System.Drawing.Size(47, 17);
            this.boldBox.TabIndex = 3;
            this.boldBox.Text = "Bold";
            this.boldBox.UseVisualStyleBackColor = true;
            // 
            // ignoreBox
            // 
            this.ignoreBox.AutoSize = true;
            this.ignoreBox.Location = new System.Drawing.Point(6, 72);
            this.ignoreBox.Name = "ignoreBox";
            this.ignoreBox.Size = new System.Drawing.Size(83, 17);
            this.ignoreBox.TabIndex = 1;
            this.ignoreBox.Text = "Ignore Case";
            this.ignoreBox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.boldBox);
            this.groupBox3.Controls.Add(this.underlineBox);
            this.groupBox3.Controls.Add(this.italicBox);
            this.groupBox3.Location = new System.Drawing.Point(12, 160);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 47);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Formatting";
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(12, 213);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(260, 23);
            this.submitBtn.TabIndex = 5;
            this.submitBtn.Text = "Apply";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // TextFormattingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 247);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TextFormattingFrm";
            this.Text = "Text Formatting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton allRadioBtn;
        private System.Windows.Forms.RadioButton currentRadioBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ignoreBox;
        private System.Windows.Forms.RichTextBox formattingTxt;
        private System.Windows.Forms.CheckBox underlineBox;
        private System.Windows.Forms.CheckBox italicBox;
        private System.Windows.Forms.CheckBox boldBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button submitBtn;
    }
}