﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Tools.Word;
using System.Runtime.InteropServices;
using System.Drawing;

namespace TBSEAssignment
{
    class WordTabPage : CustomTextTabPage
    {
        Word.Application mApplication;
        Word.Document mDocument;

        public WordTabPage(String mFilePath, Enum mType) : base(mFilePath, mType)
        {

        }

        override public void mSaveFile()
        {
            if (mFileState)
            {
                mSaveCount++;
                vRichTextBox.Invoke(new System.Action(() =>
                {
                    mDocument.Content.Text = vRichTextBox.Text;
                }));
                mDocument.Save();
                mSaveCount--;
            }
        }

        override public void mCreateFile()
        {
            mApplication = new Word.Application();
            mDocument = mApplication.Documents.Add();
            mDocument.SaveAs2(mFilePath);
        }

        override public void mReadFile()
        {
            mApplication = new Word.Application();
            mDocument = mApplication.Documents.Open(mFilePath);

            foreach (Word.Paragraph mParagraph in mDocument.Paragraphs)
            {
                vRichTextBox.Text += mParagraph.Range.Text;
            }
        }

        override public void mCloseFile()
        {
            if (MessageBox.Show("Do you want to close " + Path.GetFileName(mFilePath) + "?", "Close Document", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
            {
                Task mTask = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        if (MessageBox.Show("Do you want to save " + Path.GetFileName(mFilePath) + "?", "Save Document", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            mSaveFile();
                        }
                        while (mSaveCount != 0)
                        {

                        }
                        mFileState = false;

                        GC.WaitForPendingFinalizers();

                        mDocument.Close();
                        Marshal.ReleaseComObject(mDocument);
                        mApplication.Quit();
                        Marshal.ReleaseComObject(mApplication);
                        Parent.Invoke(new System.Action(() =>
                        {
                            Parent.Controls.Remove(this);
                        }));
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });
                mTask.Wait();
            }
        }
    }
}

